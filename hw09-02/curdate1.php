<?php
/* 
 *  ДЗ  2015-09-02
 *
 * 1. Страничка выводить текущее время, дату, и день недели (число) в формате json
 *    (например {“time”: “18:30:22”, “date”: “4.09.2015”, “dayOfWeek: “5”})
 * 2. Страница подключается к выше описаной странице, парсить данные
 *    и выводить следующее: Hello user, today is Friday, date is 15.09.2015 and time is 18:30:22
 *
 */

/***  Часть 1 ***/
$date = array(
	'time'		 => date('H:i:s'),
	'date'		 => date('j.m.Y'),
	'dayOfWeek'	 => date('N'),
);
$date = json_encode($date);
echo $date;