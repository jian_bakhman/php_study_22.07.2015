<?php

/* 
 *  ДЗ  2015-09-02
 *
 * 1. Страничка ( curdate1.php ) выводит текущее время, дату, и день недели (число) в формате json
 *    (например {“time”: “18:30:22”, “date”: “4.09.2015”, “dayOfWeek: “5”})
 * 2. Страница ( curdate2.php ) подключается к выше описаной странице, парсить данные
 *    и выводить следующее: Hello user, today is Friday, date is 15.09.2015 and time is 18:30:22
 */

/**  Часть 2 **/
$daynum = [
	'1' => 'Monday',
	'2' => 'Tuesday',
	'3' => 'Wednesday',
	'4' => 'Thursday',
	'5' => 'Friday',
	'6' => 'Saturday',
	'7' => 'Sunday',
];

$url = 'http://phpschool/hw09-02/curdate1.php';
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
$date = curl_exec($ch);
$date = json_decode($date, TRUE);
$curdate = $date['date'];
$curweekday = $date['dayOfWeek'];
$curtime = $date['time'];

echo 'Hello user, today is ' . $daynum[$curweekday] . ', date is ' . $curdate . ' and time is ' . $curtime ;