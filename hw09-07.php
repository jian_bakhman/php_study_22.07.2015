<?php
/* 
 *  ДЗ  2015-09-07
 *
 *  Поиск по аудиозаписям на pleer.com, метод - tracks_search
 */

session_start();
?>

<form method="POST">
	<label>Введите запрос</label>
	<input type="text" name="search"/>
	<input type="submit" />
</form>

<?php
if ($query = $_POST['search']){
	$url = "http://api.pleer.com/token.php";

	$ch = curl_init($url);
	curl_setopt_array($ch, [
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST => true,
		CURLOPT_USERPWD => '193508:QBsV2tdolrZ3QN3Zpgva',
		CURLOPT_POSTFIELDS => [
			"grant_type" => "client_credentials"
		],
	]);

	if($data = curl_exec($ch)) {
		$data = json_decode($data, true);
		$token = $data['access_token'];
		$_SESSION['token'] = $token;

		curl_setopt_array($ch, [
			CURLOPT_URL => 'http://api.pleer.com/index.php?token=',
			CURLOPT_HTTPHEADER => ['Authorization: Bearer ' . $token,] ,
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => [
				'method'		 => 'tracks_search',
				'query'			 => $query,
				'page'			 => 1,
				'result_on_page' => 20,
			],
		]);

		if($search = curl_exec($ch)) {
			$search = json_decode($search, true);
		}
	}
	?>

	<p>
		Запрос: <strong><?php echo $query; ?></strong><br>
		Результатов примерно: <?php echo $search['count']; ?>
	</p>
	<ul>
		<?php
		foreach ($search['tracks'] as $value) {
			echo '<li>' . $value['artist'] . ' .... ' 
					. $value['track'] . ' .... '
					. $value['lenght'] . 'c </li>';
		}
		?>
	</ul>
	
<?php }