<?php
/*
 * Создать скрипт картинной галереи. Скрипт должен содержать две страницы. 
 * На первой пользователь загружает изображение, скрипт должен проверить, 
 * что это картинка в формате jpeg, и сохранить ее в папку. 
 * На второй вывод всех загруженых изображений (тег <img src=”” ) 
 */

$uploadsFolder = '/uploads';
$uploads_dir = __DIR__ . $uploadsFolder;
if(empty($_GET) || $_GET['galery'] != 1){
    ?>
    <form method="POST" enctype="multipart/form-data">
        <input type="file" name="sfile[]" multiple=""/>
        <input type="submit" name="send"/>
    </form>
    <a href="http://phpschool/hw08-17.php?galery=1">Перейти в галерею</a>
    <?php
}
else {
    echo "Просмотр галереи <br>";
    $galery = array_diff(scandir($uploads_dir), array('.', '..'));
    foreach ($galery as $value) {
        echo '<img style="height: 150px; margin: 5px;" src="' . $uploadsFolder . '/' . $value . '">';
    }
    ?>
    <p><a href="http://phpschool/hw08-17.php">Добавить в галерею</a></p>
    <?php
}

if (!empty($_FILES)){
    foreach ($_FILES["sfile"]["error"] as $key => $error) {
        $uploadFileType = $_FILES["sfile"]["type"][$key];
        if ($error == UPLOAD_ERR_OK && substr($uploadFileType, 0, strpos($uploadFileType, "\\")) == 'image') {
            $tmp_name = $_FILES["sfile"]["tmp_name"][$key];
            $name = $_FILES["sfile"]["name"][$key];
            move_uploaded_file($tmp_name, "$uploads_dir/$name");
        }
    }
}

?>