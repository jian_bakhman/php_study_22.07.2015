<?php
/* 
 *  ДЗ  2015-09-16
 *
 *  Выполнил: ЖИАН БАХМАН
 *
 *  2. Создать класс точка (Point). У нее должны быть свойства x1, y1 и метод print,
 *     который должен выводить координаты на экран. Инкапсулировать координаты,
 *     написать get, set методы. Методы должны любые входящие данные приводить к типу int
 *
 *  3. Создать класс Line, который наследуется от класса Point, у него появляются
 *     еще два дополнительных поля x2, y2 и переопределить метод print, чтобы выводились
 *     все координаты. В таком же духе оформить класс Triangle и Rectangle
 *
 *  4. Сделать метод в классе Line - isOnLine, который будет проверять находится ли
 *     точка на прямой, на вход функция принимает объект класса Point
 *
 *  5. Повторить пройденный материал за месяц, будет контрольная.
 */

class Point {
	protected $x1;
	protected $y1;

	public function __construct($p) {	// принимает массив
		if (is_array($p)){
			$this->setPoint($p);
		} else {
			echo 'is not an array <br>';
			return FALSE;
		}
	}
	public function printPoint(){
		echo $this->x1 . ', ' . $this->y1 . '<br>';
		echo str_repeat('-', 7) . '<br>';
	}
	public function setPoint($p){
		$this->x1 = (int) $p[0];
		$this->y1 = (int) $p[1];
	}
	public function getPoint(){
		return ['x'  => $this->x1, 'y' => $this->y1] ;
	}
}

class Line extends Point{
	protected $x2;
	protected $y2;

	public function printLine(){
		echo $this->x1 . ', ' . $this->y1 . "<br>";
		echo $this->x2 . ', ' . $this->y2 . "<br>";
		echo str_repeat('-', 7) . '<br>';
	}
	public function isOnLine($pointOnLine){
		$point = $pointOnLine->getPoint();
		$this->x = $point['x'];
		$this->y = $point['y'];
		if (($this->x == $this->x1) && ($this->y == $this->y1)) {
			echo 'is on line <br>';
		} elseif (($this->x != $this->x1) && ($this->y == $this->y1)) {
			echo 'is <b>not</b> on line <br>';
		} else {
			$this->arctg1 = ($this->x2 - $this->x1) / ($this->y2 - $this->y1);
			$this->arctg2 = ($this->x  - $this->x1) / ($this->y  - $this->y1);
			if ($this->arctg1 == $this->arctg2) {
				echo 'is on line <br>';
			} else {
				echo 'is <b>not</b> on line <br>';
			}
		}
	}
	public function setPoint($p){
		$this->x1 = (int) $p[0];
		$this->y1 = (int) $p[1];
		$this->x2 = (int) $p[2];
		$this->y2 = (int) $p[3];
		if (($this->x1 == $this->x2) && ($this->y1 == $this->y2)){
			echo "it's same points<br>";
		}
	}
}

class Triangle extends Line{
	protected $x3;
	protected $y3;

	public function printTriangle(){
		echo $this->x1 . ', ' . $this->y1 . "<br>";
		echo $this->x2 . ', ' . $this->y2 . "<br>";
		echo $this->x3 . ', ' . $this->y3 . "<br>";
		echo str_repeat('-', 7) . '<br>';
	}
	public function setPoint($p){
		$this->x1 = (int) $p[0];
		$this->y1 = (int) $p[1];
		$this->x2 = (int) $p[2];
		$this->y2 = (int) $p[3];
		$this->x3 = (int) $p[4];
		$this->y3 = (int) $p[5];
	}
}

class Rectangle extends Triangle{
	private $x4;
	private $y4;

	public function printRectangle(){
		echo $this->x1 . ', ' . $this->y1 . "<br>";
		echo $this->x2 . ', ' . $this->y2 . "<br>";
		echo $this->x3 . ', ' . $this->y3 . "<br>";
		echo $this->x4 . ', ' . $this->y4 . "<br>";
		echo str_repeat('-', 7) . '<br>';
	}
	public function setPoint($p){
		$this->x1 = (int) $p[0];
		$this->y1 = (int) $p[1];
		$this->x2 = (int) $p[2];
		$this->y2 = (int) $p[3];
		$this->x3 = (int) $p[4];
		$this->y3 = (int) $p[5];
		$this->x4 = (int) $p[6];
		$this->y4 = (int) $p[7];
	}
}

$point1 = new Point([7, 10]);
$point1->printPoint();

$point2 = new Point([6, 3]);	// точка на линии
//$point2 = new Point([2, 3]);	// "y" совпадает с "y1"
//$point2 = new Point([2, 1]);	// точка совпадает с точкой на линии

$line1 = new Line ([2, 1, 8, 4]);
//$line1 = new Line ([2, 1, 2, 1]);	// точки на линии совпадают
$line1->printLine();
$line1->isOnLine($point2);	// проверка, на линии ли точка