<?php
error_reporting(E_ALL);	// настройка PHP для вывода всех ощибок и предупреждений
/**
 *  ДЗ  2015-09-28
 *
 *  Выполнил: ЖИАН БАХМАН
 *
 * 2. Создать класс с магическими методами __set, __get, __unset, __isset, который будет хранить данные
 *     во внутреннем массиве (свойство класса), но принимать на вход будет только целые числа ( $t->foo = 2 ),
 *     если ему будет передано что то другое, класс игнорирует ввод, и просто ничего не делает.
 *     Метод  __set	должен проверить, четное ли число или нет и записать в массив 0 - если четное,
 *				1 - если нет (ключ массива должен быть именем переданного свойства).
 *     Метод  __unset	должен удалять данные из внутреннего массива.
 *     Метод  __get	должен возвращать (1 или 0).
 *     Метод  __isset	должен проверять на существование.
 *
 * 3. Реализовать магический метод __toString, который должен сконкатенировать все данные и вернуть их.
 *
 * 5. Реализовать метод __invoke c следующей логикой, когда скрипт пытается выполнить объект как функцию
 *     и передает как аргумент 0 или 1 вернуть все четные или нечетные ключи массива, в остальных случаях вернуть false
 *
 */

class Data {
	private $data = [];

	public function __set($name, $value) {
		echo "test __set(): ";
		if (is_int($value)) {
			$this->data[$name] = ($value%2) ? 0 : 1;
		}
		var_dump($this->data);
		echo "<hr>";
	}

	public function __get($name) {
		echo "test __get(): ";
		return (isset($this->data[$name])) ? $this->data[$name] : FALSE;
	}

	public function __isset($name) {
		echo "test __isset(): ";
		var_dump($this->data);
		echo $name . '<br>';
		return isset($this->data[$name]) ? TRUE : FALSE ;
	}
	
	public function __unset($name) {
		echo "test __unset(): ";
		if (isset($this->data[$name])) {
			unset($this->data[$name]);
			echo "Is unset ";
			var_dump($this->data);
		}  else {
			echo "Is not set";
		}
		echo '<hr>';
	}

	public function __toString() {
		echo "test __toString(): ";
		return implode($this->data);
	}

	public function __invoke($param) {
		echo "test __invoke(): ";
		$result = '';
		if ($param == 0 || $param == 1) {
			foreach ($this->data as $key => $value) {
				$result = $result . (($value == $param) ? $key : '');
			}
			return $result;
		} else {
			return FALSE;
		}
	}
}

$d = new Data();

$d->a = 1;
$d->b = '2';
$d->c = '2 Hello';
$d->d = 4;
$d->e = 15;
$d->f = 5.12;
$d->j = 512;

$result = $d->b;
var_dump($result);
echo "<hr>";

$result = $d->c;
var_dump($result);
echo "<hr>";

$result = isset($d->b);
var_dump($result);
echo "<hr>";

unset($d->b);

echo isset($d->b);
echo "<hr>";
echo isset($d->d);
echo "<hr>";

echo $d;
echo "<hr>";

$t = $d(1);
var_dump($t);